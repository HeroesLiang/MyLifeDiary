package com.example.hasee.lifediary;

/**
 * Created by hasee on 2018/6/19.
 */

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.hasee.mylifediary.DaoMaster;
import com.example.hasee.mylifediary.DaoMaster.DevOpenHelper;
import com.example.hasee.mylifediary.DaoSession;
import com.example.hasee.mylifediary.Note;
import com.example.hasee.mylifediary.NoteDao;

import tyrantgit.explosionfield.ExplosionField;

public class DiaryEditor extends Activity {

    private static final String TAG = "Diary";
    public static final String EDIT_DIARY_ACTION = "com.example.hasee.lifediary.DiaryEditor.EDIT_DIARY";
    public static final String INSERT_DIARY_ACTION = "com.example.hasee.lifediary.DiaryEditor.action.INSERT_DIARY";
    public static final int MENU_ITEM_DELETE = Menu.FIRST + 1;



    /**
     * 查询cursor时候，感兴趣的那些条例。
     */
    // private static final String[] PROJECTION
    // = new String[] { DiaryColumns._ID, // 0
    // DiaryColumns.TITLE, DiaryColumns.BODY, // 1
    // };

    // GreenDAO使用的变量
    private SQLiteDatabase db;

    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private NoteDao noteDao;
    private Note note;
    private Cursor cursor;

    private static final int STATE_INSERT = 1;
    private int mState;
    private EditText mTitleText;
    private EditText mBodyText;
    private Button confirmButton;
    private Button cancelButton;
    private Button shareButton;
    private long mid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(android.R.style.Theme_Black);
        final Intent intent = getIntent();
        final String action = intent.getAction();
        setContentView(R.layout.diary_edit);



        mTitleText = (EditText) findViewById(R.id.title);
        mBodyText = (EditText) findViewById(R.id.body);
        confirmButton = (Button) findViewById(R.id.confirm);
        cancelButton =(Button)findViewById(R.id.cancel);
        shareButton=(Button)findViewById(R.id.share);


        initDAO();
        Log.d("action", action);

        if (EDIT_DIARY_ACTION.equals(action)) {// 编辑日记

            Bundle bundle = new Bundle();
            bundle = this.getIntent().getExtras();
            mid = bundle.getLong("id");
            // note = noteDao.load(mid);
            note = noteDao.loadByRowId(mid);

            mTitleText.setTextKeepState(note.getTitle());
            mBodyText.setTextKeepState(note.getBody());

            setTitle("编辑日记");
        } else if (INSERT_DIARY_ACTION.equals(action)) {// 新建日记
            mState = STATE_INSERT;
            setTitle("新建日记");
        } else {
            Log.e(TAG, "no such action error");
            finish();
            return;
        }

        //确定
        confirmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (mState == STATE_INSERT) {
                    insertDiary();
                } else {
                    updateDiary();
                }
                Intent intent = new Intent(DiaryEditor.this, LifeDiary.class);
                startActivity(intent);
                DiaryEditor.this.finish();
            }
        });

        //分享
        shareButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent,"分享到。。。"));
            }
        });

        //取消
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DiaryEditor.this, LifeDiary.class);
                startActivity(intent);
            }
        });

    }

    private void initDAO() {
        DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db",
                null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        noteDao = daoSession.getNoteDao();
    }

    private void insertDiary() {
        String title = mTitleText.getText().toString();
        String body = mBodyText.getText().toString();
        final DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
                DateFormat.MEDIUM);
        String date = df.format(new Date());

        Note note = new Note(null, title, body, date);
        noteDao.insert(note);

        Log.d("DaoExample", "Inserted new note, ID: " + note.getId());

    }

    private void updateDiary() {
        String title = mTitleText.getText().toString();
        String body = mBodyText.getText().toString();
        final DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
                DateFormat.MEDIUM);
        String date = df.format(new Date());

        Note note = new Note(mid, title, body, date);
        noteDao.insertOrReplace(note);

    }



}
