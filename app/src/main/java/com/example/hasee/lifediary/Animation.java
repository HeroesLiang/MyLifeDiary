package com.example.hasee.lifediary;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

/**
 * Created by hasee on 2018/6/22.
 */

public class Animation extends Activity{
    private RelativeLayout view;//声明控件
    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation);
        //初始化控件
        view = (RelativeLayout) findViewById(R.id.flower);
        //属性动画
        ObjectAnimator parentAnimator = ObjectAnimator.ofInt(
                view,"backgroundColor",
                Color.parseColor("#ff0000"),
                Color.parseColor("#0000ff"));
        parentAnimator.setEvaluator(new ArgbEvaluator());//设置颜色估值器
//        parentAnimator.setRepeatCount(ValueAnimator.INFINITE);//播放次数：循环
//        parentAnimator.setRepeatMode(ValueAnimator.REVERSE);//播放模式：循环播放时不是从头开始，从结尾开始
        parentAnimator.setDuration(3000);//播放时间3000ms，也就是3秒
        parentAnimator.start();//播放动画
        //设置一个定时器
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Animation.this,LifeDiary.class);
                startActivity(intent);
                finish();
            }
        },3000);

    }
}
